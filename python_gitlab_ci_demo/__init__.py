"""Top-level package for Python Gitlab-CI Demo."""

__author__ = """Philipp Weißmann"""
__email__ = 'mail@philipp-weissmann.de'
__version__ = '0.1.0'
