#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = []

test_requirements = [
    "pytest>=3",
]

setup(
    author="Philipp Weißmann",
    author_email="mail@philipp-weissmann.de",
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
    ],
    description="Python Example, using gitlab-ci",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="python_gitlab_ci_demo",
    name="python_gitlab_ci_demo",
    packages=find_packages(
        include=["python_gitlab_ci_demo", "python_gitlab_ci_demo.*"]
    ),
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/derphilipp/python_example",
    version="0.1.0",
    zip_safe=False,
)
