#!/usr/bin/env python

"""Tests for `python_gitlab_ci_demo` package."""


from python_gitlab_ci_demo import python_gitlab_ci_demo


def test_one_and_one_is_two():
    # Arrange
    a, b = 1, 1
    # Act
    result = python_gitlab_ci_demo.add(a, b)
    # Assert
    assert result == 2
