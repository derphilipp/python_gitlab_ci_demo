=====================
Python Gitlab-CI Demo
=====================


.. image:: https://img.shields.io/pypi/v/python_gitlab_ci_demo.svg
        :target: https://pypi.python.org/pypi/python_gitlab_ci_demo

.. image:: https://img.shields.io/travis/derphilipp/python_gitlab_ci_demo.svg
        :target: https://travis-ci.com/derphilipp/python_gitlab_ci_demo

.. image:: https://readthedocs.org/projects/python-gitlab-ci-demo/badge/?version=latest
        :target: https://python-gitlab-ci-demo.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Python Example, using gitlab-ci


* Free software: MIT license
* Documentation: https://python-gitlab-ci-demo.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
